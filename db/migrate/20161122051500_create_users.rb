class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.text :image_path
      t.decimal :latitude, default: 0, precision: 10, scale: 2
      t.decimal :longitude, default: 0, precision: 10, scale: 2
      t.timestamps null: false
    end
  end
end
