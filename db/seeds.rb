# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = [{
          name: 'user1',
          image_path: "http://placehold.it/32x32?text=user1",
          latitude: 43,
          longitude: 3.5
        },
        {
          name: 'user2',
          image_path: "http://placehold.it/32x32?text=user2",
          latitude: 45,
          longitude: 4
        },
        {
          name: 'user3',
          image_path: "http://placehold.it/32x32?text=user3",
          latitude: 43,
          longitude: 3.5
        },
        {
          name: 'user4',
          image_path: "http://placehold.it/32x32?text=user4",
          latitude: 49,
          longitude: 4
        },
        {
          name: 'user5',
          image_path: "http://placehold.it/32x32?text=user5",
          latitude: 51,
          longitude: 3.5
        }]
users.each do |user|
  curr_user = User.find_by_name(user[:name])
  unless curr_user.present?
    User.create(user)
  end
end
