# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
  handler = Gmaps.build('Google')
  handler.buildMap {
    provider:
      minZoom: 2
      maxZoom: 15
      fullscreenControl: true
      scaleControl: true
    internal: id: 'multi_markers'
  }, ->
    markers = handler.addMarkers([
      {
        lat:  23.0332969
        lng: 72.554502
      }
      {
        lat: 22.835602
        lng: 74.255989
      }
      {
        lat: 23.302189
        lng: 81.356804
      }
      {
        lat: 19.990751
        lng: 72.743675
      }
      {
        lat: 27.995020
        lng: 74.961792
      }
    ])
    handler.map.centerOn new (google.maps.LatLng)(23.0332969, 72.554502)
    handler.bounds.extendWith markers
    handler.fitMapToBounds()
    return
  return
