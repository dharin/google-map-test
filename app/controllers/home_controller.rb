class HomeController < ApplicationController
  def index
    @users = User.all
    @hash = Gmaps4rails.build_markers(@users) do |user, marker|
      marker.lat user.latitude
      marker.lng user.longitude
      marker.picture({
                      :url => "#{user.image_path.present? ? user.image_path : 'http://placehold.it/32x32?text=avtar'}",
                      :width   => 32,
                      :height  => 32
                     })
      marker.title user.name
    end
  end
end
